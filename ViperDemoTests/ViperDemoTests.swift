//
//  ViperDemoTests.swift
//  ViperDemoTests
//
//  Created by Nguyen Tuong Vi on 7/31/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import XCTest
@testable import ViperDemo

class ViperDemoTests: XCTestCase {
    
    var postListInterator = PostListInteractor()
    var postList:[Post] = []
    
    override func setUp() {
        super.setUp()
        postList = postListInterator.getPostList()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetPostList() {
        XCTAssert(postList.count > 0, "Cannot get products")
    }
    
    func testCountPostList() {
        
        XCTAssert(postList.count == 5, "Not enough products")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
