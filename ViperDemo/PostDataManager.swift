//
//  PostDataManager.swift
//  ViperDemo
//
//  Created by Nguyễn Tường Vi on 8/1/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import Foundation

//class PostDataManager {
//    
//    static let shared = PostDataManager()
//    
//    struct Errors {
//        static let invalidAccessErrorCode = 100
//    }
//    
//    struct FlickrAPIMetadataKeys {
//        static let failureStatusCode = "code"
//    }
//    
//    struct FlickrAPI {
//        
//        static let APIKey = "5153b0aea4afa384d5e72affc6a53390"
//        static let tagsSearchFormat = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=14994975868e510312ec4f51dfc7701a&tags=Party&page=1&format=json&nojsoncallback=1"
//    }
//    
//    func fetchPhotosForSearchText(searchText: String, page: NSInteger, clousure: @escaping (NSError, NSInteger, [Post]?) -> Void) -> Void {
//            
//            let escapedSearchText = searchText.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
//            let format = FlickrAPI.tagsSearchFormat
//            let arguments : [CVarArg] = [FlickrAPI.APIKey, escapedSearchText!, page]
//            let photoUrl = String(format: format, arguments)
//            let url = URL(string: photoUrl)
//            let request = URLRequest(url: url!)
//            let searchTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
//                
//                if error != nil {
//                    print(error?.localizedDescription ?? "")
//                    clousure(error! as NSError, 0, nil)
//                }
//                
//                do {
//                    
//                    let resultsDict = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
//                    guard let results = resultsDict else { return }
//                    if let statusCode = results[FlickrAPIMetadataKeys.failureStatusCode] as? Int {
//                        if statusCode == Errors.invalidAccessErrorCode {
//                            let invalidAccessError = NSError(domain: "FlickrAPIDomain", code: statusCode, userInfo: nil)
//                            clousure(invalidAccessError, 0, nil)
//                            return
//                        }
//                    }
//                    
//                    guard let photoContainers = resultsDict!["photos"] as? NSDictionary else { return }
//                    guard let totalPageCountString = photoContainers["total"] as? String else { return }
//                    guard let totalPageCount = NSInteger(totalPageCountString as String) else { return }
//                    guard let photosArray = photoContainers["photo"] as? [NSDictionary] else { return }
//                    
//                    let flickrPhotos : [Post] = photosArray.map({ (photoDict) -> Post in
//                        
//                        let photoId = photoDict["id"] as? String ?? ""
//                        let farm = photoDict["farm"] as? Int ?? 0
//                        let secret = photoDict["secret"] as? String ?? ""
//                        let server = photoDict["server"] as? String ?? ""
//                        let title = photoDict["title"] as? String ?? ""
//                        
//                        let flickrPhoto = Post(photoId: photoId, farm: farm, secret: secret, server: server, title: title)
//                        
//                        return flickrPhoto
//                    })
//                    
////                    clousure(nil, totalPageCount, flickrPhotos)
//                    
//                } catch let error as NSError {
//                    print("Error parsing JSON: \(error)")
//                    clousure(error as NSError, 0, nil)
//                    return
//                }
//            }
//            searchTask.resume()
//        }
//    }
