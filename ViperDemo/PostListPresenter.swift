//
//  PostListPresenter.swift
//  ViperDemo
//
//  Created by Nguyễn Tường Vi on 8/1/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit
import PKHUD
import FileBrowser

class PostListPresenter: PostListPresenterProtocol {
   

    //Var
    weak var view : PostListViewProtocol?
    var interactor : PostListInteractorInputProtocol?
    var wireFrame: PostListWireframeProtocol?
    
    
    func viewDidLoad() {
        view?.showLoading()
        interactor?.retrievePostList()
    }
    
    func showPostDetail(forPost post: Post) {
        wireFrame?.presentPostDetailScreen(from: view!, forPost: post)
    }
    
    func showCartProduct(forPost post: Post) {
        wireFrame?.presentCartProductScreen(from: view!, forPost: post)
    }
    
    func showMoreAction(postIndex : IndexPath, vc : PostListView, url : String) {
        
        let alert = UIAlertController(title: "More Action", message: "Do you want another action?", preferredStyle: .actionSheet)
        
        let DownloadAction = UIAlertAction(title: "Download Image", style: .default) { (action) in
            self.downloadImg(urlString: url, vc: vc)
        }
        
        let CoppyLinkAction = UIAlertAction(title: "Coppy Link", style: .default) { (action) in
            
        }
        
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alert.addAction(DownloadAction)
        alert.addAction(CoppyLinkAction)
        alert.addAction(CancelAction)
        
        vc.present(alert, animated: true, completion: nil)
    }
    
    func downloadImg(urlString: String, vc : PostListView) {
        
        HUD.show(.progress)
        guard let url = URL(string: urlString) else { return }
        let fileURL = self.getDocumentsDirectory().appendingPathComponent("ViperArchitechture").appendingPathComponent(url.lastPathComponent)
        
        DispatchQueue.global().async {
            
            do {
                let data = try Data(contentsOf: url)
                try self.createFolder(folderName: "ViperDemo")
                try data.write(to: fileURL)
                
                DispatchQueue.main.async {
                    let fileBrowser = FileBrowser(initialPath: self.getDocumentsDirectory().appendingPathComponent("ViperDemo"), allowEditing: true, showCancelButton: true)
                    vc.present(fileBrowser, animated: true, completion: nil)
                    HUD.hide(afterDelay: 0)
                }
            } catch (let err) {
                HUD.hide(afterDelay: 0)
                print(err.localizedDescription)
            }
        }
    }
    
}

extension PostListPresenter : PostListInteractorOutputProtocol {
    
    func didRetrievePosts(from post: [Post]) {
        view?.hideLoading()
        view?.showPost(post: post)
    }
    
    func onError() {
        view?.hideLoading()
        view?.showError()
    }
}
extension PostListPresenter {
    
    func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func createFolder(folderName:String) throws {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = paths[0]
        let folderUrl = documentDirectory.appendingPathComponent(folderName)
        if !FileManager.default.fileExists(atPath: folderUrl.absoluteString) {
            try FileManager.default.createDirectory(at: folderUrl, withIntermediateDirectories: true, attributes: nil)
        }
    }

}

