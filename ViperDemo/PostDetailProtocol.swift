//
//  PostDetailProtocol.swift
//  ViperDemo
//
//  Created by Nguyễn Tường Vi on 8/2/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

protocol PostDetailViewProtocol : class {
    
    var presenter : PostDetailPresenterProtocol? { get set}
    //PRESENTER -> VIEW
    func showPostDetail(forPost post: Post)
}

protocol PostDetailPresenterProtocol : class {
    
    var view : PostDetailViewProtocol? { get set}
    var wireframe : PostDetailWireFrameProtocol? { get set}
    var post: Post? { get set}
    
    //VIEW -> PRESENTER
    func viewDidLoad()
    
}

protocol PostDetailWireFrameProtocol : class {
    
    static func createPostDetailModule(forPost post: Post) -> UIViewController
}
