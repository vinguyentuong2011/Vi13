//
//  PostDetailWireframe.swift
//  ViperDemo
//
//  Created by Nguyễn Tường Vi on 8/2/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

class PostDetailWireframe: PostDetailWireFrameProtocol {
    
    static var mainStoryboard : UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    static func createPostDetailModule(forPost post: Post) -> UIViewController {
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "PostDetailVC")
        if let view = viewController as? PostDetailView {
            let presenter : PostDetailPresenterProtocol = PostDetailPresenter()
            let wireframe : PostDetailWireFrameProtocol = PostDetailWireframe()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireframe = wireframe
            presenter.post = post
            
            return viewController
        }
        return UIViewController()
    }
}
