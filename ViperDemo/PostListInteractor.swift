//
//  PostListInteractor.swift
//  ViperDemo
//
//  Created by Nguyễn Tường Vi on 8/1/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import Foundation

class PostListInteractor : PostListInteractorInputProtocol {

    weak var presenter: PostListInteractorOutputProtocol?
    var localDatamanager: PostListLocalDataManagerInputProtocol?
    
    func getPostList() -> [Post] {

        let postModelList = [
                    Post(title: "iPad Pro", imageUrl: "https://cdn2.macworld.co.uk/cmsdata/reviews/3580947/ipad_pro_review_video_1200.jpg", thumbImageUrl: "https://images-na.ssl-images-amazon.com/images/I/41PO9iRdo7L._SL500_AC_SS350_.jpg", description: "iOS 11 brings iPad to life like never before. New features and capabilities let you get more done, more quickly and easily", price: "$500"),
                    
                    Post(title: "iMac 2017", imageUrl: "https://www.apple.com/v/imac/e/images/overview/og_image.png?201706211338", thumbImageUrl: "https://www.bhphotovideo.com/images/images2500x2500/apple_mk142ll_a_21_5_imac_late_2015_1190344.jpg", description: "Pros love iMac. So when they asked us to build them a killer iMac, we went all in. And then we went way, way beyond", price: "$2000"),
                    
                    Post(title: "AppleWatch Seri 2", imageUrl: "http://photos2.insidercdn.com/apple-watch-1.jpg", thumbImageUrl: "https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP746/alu-rosegold.jpg", description: "The culmination of a partnership based on parallel thinking, singular vision, and mutual regard", price: "$620"),
                    Post(title: "Apple TV Gen 3", imageUrl: "https://support.apple.com/library/content/dam/edam/applecare/images/en_US/appletv/apple-tv-siri-search-hero.jpg", thumbImageUrl: "https://target.scene7.com/is/image/Target/14051076_Alt04?wid=520&hei=520&fmt=pjpeg", description: "The Apple TV Remote has a precise Touch surface, so you can use your thumb to quickly and accurately navigate around your television screen.", price: "$430"),
                    Post(title: "Macbook Pro 2016", imageUrl: "https://boygeniusreport.files.wordpress.com/2016/10/macbook-pro-2016.jpg?quality=98&strip=all", thumbImageUrl: "https://www.bhphotovideo.com/images/images2500x2500/apple_mlh12ll_a_13_3_macbook_pro_with_1293725.jpg", description: "The Touch Bar replaces the function keys that have long occupied the top of your keyboard with something much more versatile and capable.", price: "$3100")
                ]
        
        return postModelList
    }
    
    func retrievePostList() {
        presenter?.didRetrievePosts(from: self.getPostList())
    }
}

extension PostListInteractor : PostListLocalDataManagerOutputProtocol {
    
    func onPostRetrieved(post: [Post]) {
        presenter?.didRetrievePosts(from: post)
        for post in post {
            
           do { try localDatamanager?.savePost(title: post.title, description: post.description, imageUrl: post.imageUrl, thumbImageUrl: post.thumbImageUrl)
            } catch {
                
            }
        }
    }
    
    func onError() {
        presenter?.onError()
    }
}
