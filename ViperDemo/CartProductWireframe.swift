//
//  CartProductWireframe.swift
//  ViperDemo
//
//  Created by Nguyễn Tường Vi on 8/3/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

class CartProductWireframe: CartProductWireframeProtocol {
    
    static var mainStoryboard : UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    static func createCartProductModule(forPost post: Post) -> UIViewController {
        let cartProductVC = mainStoryboard.instantiateViewController(withIdentifier: "CartProductVC")
        if let view = cartProductVC as? CartProductView {
            let presenter : CartProductPresenterProtocol = CartProductPresenter()
            let wireframe : CartProductWireframeProtocol = CartProductWireframe()
            
            presenter.view = view
            presenter.post = post
            presenter.wireframe = wireframe
            
            view.presenter = presenter
            return cartProductVC
        }
        return UIViewController()
    }
    
    func presentPostDetailScreen(from view: CartProductViewProtocol, forPost post: Post) {
        
        let postDetailVC = PostDetailWireframe.createPostDetailModule(forPost: post)
        if let sourceView = view as? UIViewController {
            sourceView.navigationController?.pushViewController(postDetailVC, animated: true)
        }

    }
}
