//
//  PostListLocalDataManager.swift
//  ViperDemo
//
//  Created by Nguyễn Tường Vi on 8/1/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import Foundation

class PostListLocalDataManager : PostListLocalDataManagerInputProtocol {
    
    func retrievePostList() throws -> [Post] {
        return [
//            Post(title: "iPad Pro", imageUrl: "https://cdn2.macworld.co.uk/cmsdata/reviews/3580947/ipad_pro_review_video_1200.jpg", thumbImageUrl: "https://images-na.ssl-images-amazon.com/images/I/41PO9iRdo7L._SL500_AC_SS350_.jpg", description: "iOS 11 brings iPad to life like never before. New features and capabilities let you get more done, more quickly and easily, making it a phenomenally powerful and personal experience."),
//            
//            Post(title: "iMac 2017", imageUrl: "https://www.apple.com/v/imac/e/images/overview/og_image.png?201706211338", thumbImageUrl: "https://www.bhphotovideo.com/images/images2500x2500/apple_mk142ll_a_21_5_imac_late_2015_1190344.jpg", description: "Pros love iMac. So when they asked us to build them a killer iMac, we went all in. And then we went way, way beyond, creating an iMac packed with the most staggeringly powerful collection of workstation-class graphics, processors, storage, memory, and I/O of any Mac ever."),
//            
//            Post(title: "AppleWatch Seri 2", imageUrl: "https://media.dcrainmaker.com/images/2016/09/Screen-Shot-2016-09-07-at-7.53.03-PM_thumb.png", thumbImageUrl: "https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/SP746/alu-rosegold.jpg", description: "The culmination of a partnership based on parallel thinking, singular vision, and mutual regard, Apple Watch Hermès is a unique timepiece designed with both utility and beauty in mind.")
        
        ]
    }
    
    func savePost(title: String, description: String, imageUrl: String, thumbImageUrl: String) throws {
        
    }
}
