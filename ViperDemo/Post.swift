//
//  Post.swift
//  ViperDemo
//
//  Created by Nguyen Tuong Vi on 7/31/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

struct Post {
    
    var title : String = ""
    var imageUrl : String = ""
    var thumbImageUrl : String = ""
    var description : String = ""
    var price : String = ""
    
    init (title : String, imageUrl : String, thumbImageUrl : String, description : String, price : String) {
        
        self.title = title
        self.imageUrl = imageUrl
        self.thumbImageUrl = thumbImageUrl
        self.description = description
        self.price = price
    }
}
