//
//  PostDetailView.swift
//  ViperDemo
//
//  Created by Nguyen Tuong Vi on 7/31/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import UIKit

class PostDetailView: UIViewController {
    
    //Outlet
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postTitleLb: UILabel!
    @IBOutlet weak var postDescriptLb: UILabel!
    
    var presenter : PostDetailPresenterProtocol?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension PostDetailView : PostDetailViewProtocol {
    
    func showPostDetail(forPost post: Post) {
        postTitleLb.text = post.title
        postDescriptLb.text = post.description
        postImage.kf.setImage(with: URL(string: post.imageUrl))
    }
}
