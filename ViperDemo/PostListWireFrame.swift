//
//  PostListWireFrame.swift
//  ViperDemo
//
//  Created by Nguyễn Tường Vi on 8/1/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

class PostListWireFrame: PostListWireframeProtocol {
   
    static var mainStoryboard : UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    static func createPostListModule() -> UIViewController {
        let navController = mainStoryboard.instantiateViewController(withIdentifier: "PostsNavigationController")
        if let view = navController.childViewControllers.first as? PostListView {
            let presenter : PostListPresenterProtocol & PostListInteractorOutputProtocol = PostListPresenter()
            let interactor : PostListInteractorInputProtocol & PostListLocalDataManagerOutputProtocol = PostListInteractor()
            let localDataManager : PostListLocalDataManagerInputProtocol = PostListLocalDataManager()
            let wireFrame : PostListWireframeProtocol = PostListWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.interactor = interactor
            presenter.wireFrame = wireFrame
            interactor.presenter = presenter
            interactor.localDatamanager = localDataManager
            
            return navController
        }
        return UIViewController()
    }
    
    func presentPostDetailScreen(from view: PostListViewProtocol, forPost post: Post) {
        
        let postDetailVC = PostDetailWireframe.createPostDetailModule(forPost: post)
        if let sourceView = view as? UIViewController {
            sourceView.navigationController?.pushViewController(postDetailVC, animated: true)
        }
    }
    
    func presentCartProductScreen(from view: PostListViewProtocol, forPost post: Post) {
        
        let cartProductVC = CartProductWireframe.createCartProductModule(forPost: post)
        if let sourceView = view as? UIViewController {
            sourceView.navigationController?.pushViewController(cartProductVC, animated: true)
        }
    }
    
    
}
