//
//  PostListViewCell.swift
//  ViperDemo
//
//  Created by Nguyen Tuong Vi on 7/31/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import UIKit
import Kingfisher



class PostListViewCell: UITableViewCell {
    
    //Var
    var indexPath : IndexPath!
    weak var delegate : PostListViewCellProtocol?
    
    
    //Outlet
    
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postTitleLb: UILabel!
    @IBOutlet weak var descript: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(post: Post)  {
        self.selectionStyle = .none
        postTitleLb.text = post.title
        descript.text = post.description
        let url = post.thumbImageUrl
        self.postImage.kf.setImage(with: URL(string: url))
    }
    
    @IBAction func cartBtn(_ sender: Any) {
        delegate?.cartBtnTapped(cell: self, indexPath: indexPath)
    }
    
    @IBAction func moreBtn(_ sender: Any) {
        delegate?.moreBtnTapped(cell: self, indexPath: indexPath)
    }
    
}
