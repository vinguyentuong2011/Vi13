//
//  CartProductProtocol.swift
//  ViperDemo
//
//  Created by Nguyễn Tường Vi on 8/3/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

protocol CartProductViewProtocol {
    var presenter : CartProductPresenterProtocol? { get set}
    //PRESENTER -> VIEW
    func showCartProduct(fromPost post: Post)
}

protocol CartProductPresenterProtocol : class {
    
    var view : CartProductViewProtocol? { get set}
    var wireframe : CartProductWireframeProtocol? { get set}
    var post: Post? {get set}
    
    //VIEW -> PRESENTER
    func viewDidLoad()
    func showPostDetail(forPost post: Post)
}

protocol CartProductWireframeProtocol : class {
    static func createCartProductModule(forPost post: Post) -> UIViewController
    
    //PRESENTER -> WIREFRAME
    func presentPostDetailScreen(from view: CartProductViewProtocol, forPost post: Post)
    
}
