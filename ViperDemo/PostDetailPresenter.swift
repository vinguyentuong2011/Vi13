//
//  PostDetailPresenter.swift
//  ViperDemo
//
//  Created by Nguyễn Tường Vi on 8/2/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import Foundation

class PostDetailPresenter: PostDetailPresenterProtocol {
    
    var view: PostDetailViewProtocol?
    var wireframe: PostDetailWireFrameProtocol?
    var post: Post?
    
    func viewDidLoad() {
        view?.showPostDetail(forPost: post!)
    }
}
