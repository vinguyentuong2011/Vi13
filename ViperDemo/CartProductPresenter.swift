//
//  CartProductPresenter.swift
//  ViperDemo
//
//  Created by Nguyễn Tường Vi on 8/3/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import Foundation

class CartProductPresenter : CartProductPresenterProtocol {

    
    var view: CartProductViewProtocol?
    var wireframe: CartProductWireframeProtocol?
    var post: Post?
    
    func viewDidLoad() {
        view?.showCartProduct(fromPost: post!)
    }
    
    func showPostDetail(forPost post: Post) {
        wireframe?.presentPostDetailScreen(from: view!, forPost: post)
    }

}
