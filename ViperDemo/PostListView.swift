//
//  PostListView.swift
//  ViperDemo
//
//  Created by Nguyen Tuong Vi on 7/31/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import UIKit
import PKHUD


class PostListView: UIViewController {
    
    //Var
    var postList : [Post] = []
    var presenter : PostListPresenterProtocol?
    
    static let identifier = "PostListView"
    class func newVC() ->  PostListView {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyBoard.instantiateViewController(withIdentifier: identifier) as! PostListView
        return vc
    }
    
    
    //Outlet
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.estimatedRowHeight = 150
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        presenter?.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension PostListView : PostListViewProtocol {
    
    func showPost(post: [Post]) {
        postList = post
        tableView.reloadData()
    }
    
    func showLoading() {
        HUD.show(.progress)
    }
    
    func hideLoading() {
        HUD.hide()
    }
    
    func showError() {
        HUD.flash(.label("Internet not connecting"), delay: 2.0)
    }
}

extension PostListView : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "PostCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PostListViewCell
        let post = postList[indexPath.row]
        cell.set(post: post)
        cell.indexPath = indexPath
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
        presenter?.showPostDetail(forPost: postList[indexPath.row])
    }
    
}

extension PostListView : PostListViewCellProtocol {
    
    func cartBtnTapped(cell: UITableViewCell, indexPath: IndexPath) {
        presenter?.showCartProduct(forPost: postList[indexPath.row])
    }
    
    func moreBtnTapped(cell: UITableViewCell, indexPath: IndexPath) {
        let post = postList[indexPath.row]
        presenter?.showMoreAction(postIndex: indexPath, vc: self, url: post.thumbImageUrl)
    }
    
    func downloadImage(cell: UITableViewCell, indexPath: IndexPath) {
        let post = postList[indexPath.row]
        presenter?.downloadImg(urlString: post.thumbImageUrl, vc: self)
    }
}
