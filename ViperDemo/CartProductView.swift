//
//  CartProductView.swift
//  ViperDemo
//
//  Created by Nguyễn Tường Vi on 8/3/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import UIKit

class CartProductView: UIViewController {
    
    
    //Var
    var presenter : CartProductPresenterProtocol?
    var postProduct : Post?
    
    //Outlet
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        presenter?.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension CartProductView : CartProductViewProtocol {
    
    func showCartProduct(fromPost post: Post) {
        self.postProduct = post
        tableView.reloadData()
    }
}

extension CartProductView : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIndentifier = "CartProductViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as! CartProductViewCell
        cell.fillData(post: postProduct!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showPostDetail(forPost: postProduct!)
    }
}
