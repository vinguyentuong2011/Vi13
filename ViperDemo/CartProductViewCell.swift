//
//  CartProductViewCell.swift
//  ViperDemo
//
//  Created by Nguyễn Tường Vi on 8/3/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import UIKit


class CartProductViewCell: UITableViewCell {
    
    //Var
    
    
    //Outlet
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productNameLb: UILabel!
    @IBOutlet weak var productDescriptLb: UILabel!
    @IBOutlet weak var priceLb: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillData(post: Post) {
        productImg.kf.setImage(with: URL(string: post.thumbImageUrl))
        productNameLb.text = post.title
        productDescriptLb.text = post.description
        priceLb.text = post.price
    }
}
