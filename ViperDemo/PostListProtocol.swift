//
//  PostListProtocol.swift
//  ViperDemo
//
//  Created by Nguyen Tuong Vi on 7/31/17.
//  Copyright © 2017 Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

protocol PostListViewProtocol: class {
    
    var presenter : PostListPresenterProtocol? { get set }
    
    //PRESENTER -> VIEW
    
    func showPost(post : [Post])
    func showError()
    func showLoading()
    func hideLoading()
}

protocol PostListPresenterProtocol: class {
    var view: PostListViewProtocol? { get set }
    var interactor: PostListInteractorInputProtocol? { get set }
    var wireFrame: PostListWireframeProtocol? { get set }
    
    // VIEW -> PRESENTER
    func viewDidLoad()
    func showPostDetail(forPost post: Post)
    func showCartProduct(forPost post: Post)
    func showMoreAction(postIndex : IndexPath, vc : PostListView, url : String)
    func downloadImg(urlString : String, vc : PostListView)
}


protocol PostListWireframeProtocol : class {
    static func createPostListModule() -> UIViewController
    
    //PRESENT -> WIREFRAME
    
    func presentPostDetailScreen(from view: PostListViewProtocol, forPost post: Post)
    func presentCartProductScreen(from view: PostListViewProtocol, forPost post: Post)
}

protocol PostListInteractorOutputProtocol : class{
    
    //INTERACTOR -> PRESENTER
    func didRetrievePosts(from post: [Post])
    func onError()
    
}

protocol PostListInteractorInputProtocol: class {
    var presenter: PostListInteractorOutputProtocol? { get set }
    var localDatamanager: PostListLocalDataManagerInputProtocol? { get set }

    
    // PRESENTER -> INTERACTOR
    func retrievePostList()
}


protocol PostListDataManagerInputProtocol : class {
    //INTERACTOR -> DATAMANAGER
}

protocol PostListLocalDataManagerInputProtocol {
    //INTERACTOR -> LOCALDATAMANAGER
    func retrievePostList() throws -> [Post]
    func savePost(title: String, description: String, imageUrl: String, thumbImageUrl: String) throws
}

protocol PostListLocalDataManagerOutputProtocol : class {
    
    //LOCALDATAMANAGER -> INTERACTOR
    func onPostRetrieved(post: [Post])
    func onError()
}

protocol PostListViewCellProtocol : class {
    
    func cartBtnTapped(cell : UITableViewCell, indexPath : IndexPath)
    func moreBtnTapped(cell : UITableViewCell, indexPath : IndexPath)
    func downloadImage(cell : UITableViewCell, indexPath : IndexPath)
}

